**dEarth is a massively multiplayer game where items, land ownership, crafting and skill learning are hold by smart contracts. PvP and PvE takes place in an open world sustained by a decentralized economy**

I have a question for you…  
What would happen if big data was used to help humanity ?  
What would you do if you were able to impact positively the real world with a virtual world ?  
How would you contribute to such a project ?  

--------------------------
## Synopsis
`Humans` are not monkeys anymore. `Earth` is sometimes sick and magic forces are unpredictable.  
**Evolve**


--------------------------
GAMEPLAY AND FEATURES
PVP
Competing is the funniest aspect of a game.
Fight and measure your skill in the arena!
Get world ranked to show your friends you are the highest skilled
Who will be the fighter of the century ?

OPEN WORLD
A finite sphere floating in the galaxy…
Fully Editable from the core to the space border
Populated with vreal PNJ, fauna and flora
Sometimes disturbed by magic forces…

SOCIAL
A revolutionary way to interact
We bring new step in how politics works.
Trade, debate, brainstorm, vote, bet…
Let’s experience pure democracy.

CRAFT
Evolving is about engineering.
Farm, craft and create machines which process your resources.
Participate to the first decentralized economy


--------------------------
DECENTRALIZED ECONOMY
TRACEABILITY
Every piece of land, every item and skill belong to someone
XP CryptoCurrency used to trade

SMART CRAFTING
Crafting, farming, engineering are done via SmartContracts
The Decentralized Economy can run outside the game

STABILITY
dEarth acts as a central bank to guarantee XP stability
It manages monetary mass depending of humans mass

VREAL MONEY
Invest in dEconomy with your clan
Make real profit from playing


--------------------------
OPEN STRATEGY
OPEN DATA
Personal data shouldn’t be sold
It should be used to understand the world
Open database with public API

OPEN FINANCE
Every revenue and expense are public
Salaries are set by the community
Investments are openly discussed and agreed

OPEN MANAGEMENT
Mixed team of internal and external staff chosen by community
Open JIRA for public workflow
Open time tracking to get open Key Performance Indicators

OPEN GOVERNANCE
Shared voting right with community
Shared property of real world investments
Shared vision of further developments


--------------------------
THE ICO
Participating in dEarth ICO is not only investing in decentralized economy but it's also giving a chance to a new democracy system to born, it's investing in the world of tomorrow.

Bonuses
To reward earliest investors whom take the more risks, every step will offer a 100% bonus on the next one: 
1 XPa = 2XPb = 4XPc and so on until we release the blockchain and the XP cryptocurrency.

Rates
1ETH = 464 USD (30/07)
1ETH = 4640 XPa
1XPa = 0.10USD (30/07)

Hard Cap – Funding goal
33,000 USD
330,000 XPa
71.12 ETH

Dates
30th of July 2018
30th of October 2018
3 months


--------------------------
ABOUT ME
I want this project to be taken by the community, I want the highest skilled people to work on this, together. Without pre-existing team, we are free to create the DreamTeam.  
I'm a professional developer and I play video games since I can hold a joystick, I do prefer open worlds and freedom than scenario and story.
I'm an over creative person and this make me able to change an unsolvable problem into a solution, it's so rewarding!
My motto is: 1 hour = 1 new idea.
I'm respectful, I believe in non-violent way of interacting. I strongly believe that humanity stands for peace and that it's just a matter of evolving.
My main life Goal is to help humanity to build a peaceful and free society. I believe that a real and uncorruptible democracy will give us the ability to live in sustainable peace.
Internet is a gift to humanity and dEarth is a laboratory for build a new set of tools which will change the world.

I'm french and I live in Thaïland, I would prefer to hide my real identity right now as I'm not sure about legality of this ICO (KYC, SEC compliant, etc...).


--------------------------
WORKFLOW
>>Read the full lightpaper here and access to the workflow<<
https://gitlab.com/l1br3/dEarth

DISCORD
Join the dEarth community on dIscord
https://discordapp.com/channels/471659163748139028

MY PHONE
Feel free to call me on whattsapp if you have any question or just want to check if I’m a real human. (english and french)
+66 98 970 8275 

EMAIL
Send me an email at coo@kapital.games
