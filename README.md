## ~~ Not modified/Original reference

## ~ Modified/My ideas to compare


# dEarth ~~


**TL;DR**  ~~
**dEarth is a massively multiplayer game where items, land ownership, crafting and skill learning are hold by smart contracts. PvP and PvE takes place in an open world sustained by a decentralized economy** ~~

*Feel free to open issues for discuting about the project or asking any question.* ~~

-------------------------- ~~

## Genese ~~
My dream when I was a child was to make a video game. From 12 to 21 I strongly believed that it was impossible to do it because people told me it was too hard. ~~
At 21 I understood that people beliefs shouldn't be mine and I started to learn code to make a game.  ~~
I always thought it would be easier to make a traditional business first (service app) to have money to invest in video game industry. ~~
At 29, I'm starting the only game I want to develop since ever, this is my plan for the next 10 years, I'll do it with or without the ICO's success.~~
My dad gave me the passion for understanding how the financial sectors works and I also worked for its company.~~

/* Will add mine here later ~

--------------------------
## Synopsis ~~ 
`Humans` are not monkeys anymore. `Earth` is sometimes sick and magic forces are unpredictable.  ~~
**Evolve** ~~
## /* Can brainstorm lots of slogans and catchy sentences here for marketing

-------------------------- ~~

## Terminology  ~~
- `dEarth` refers as the game itself ~~
- `Kapital` refers at the company whom develop and own the game ~~
- `XP` refers at `dEarth` cryptocurrency used to buy and process smart contracts ~~
- `earth` or `nature` refers to the authoritarive server ~~
- `players` are the real person holding the `XP` wallet ~~
- `humans` are the players' characters ~~

## Terminology  ~~
- `Đearth` refers as the game itself ~
- `Kapital` refers at the company whom develop and own the game ~~ 							(Actually unsure what you mean by this) Is this a current real company? Or the proposed dev team financer's company? Either way, I propose the name be Dappital/Dapital for staying with decentralized terms and just the common use of Dapp.
- `DTHC` refers at `Đearth` cryptocurrency used to buy and process smart contracts ~					This is a reference to the real definition of dearth, playing on english words with dirt. Please tel l me you mean this is seperate from In-game currency, and that players aren't forced to buy this to play the game. Also that players who DO buy it aren't making their characters A LOT stronger just because of it. This should be ONLY for voting reasons which more loyal players will be the ones buying for purposes of setting up guild factions and localised marketplaces in-game. DTHC (you say XP) should be only for EXTERNAL MICROSTRANSACTIONS that do influence the game, but not so much that it's unfair for free players. DTHC should be the main token asset linked to the game, then an in-game currency Gold for example will be something else entirely. ~
- `earth` or `nature` refers to the authoritarive server ~~										`I dont think you need to mention this to people`
- `players` are the real person holding the `XP` wallet ~~										`We chat about this, and maybe can be changed so it's an external script that indexes a chain & publishes to HTML`
- `humans` are the players' characters ~~											`See below for concept idea to talk about humans just being named factions of Ra, Apep & Aker. Specific Egyptian gods that correlate with (Sun), (Night), (Earth & Death)`

- `factions` are the players' "teams" ~							
- `outskirts` refers to the expanding world, where new PvE is created ~	
- `Char UI` player's character Skill/Stat/Battle info User Interface where your Skill points, Battle info & Base stats are shown ~	


--------------------------
## Gameplay and features

### pvp, factions & threats ~
~~ The **Fight** release will be the first public version of dEarth as a PvP **skill** game where `humans` can fight for XP. `Earth` distribute to each human a small amount of vXP (virtual, not blockchain) and `humans` have to fight to get other `humans` vXP. They can buy items and skills to `earth` shop to increase their power and unlock new features.

~ 1 Hero initially for testing purposes, will not afford time for creation of all until Alpha.

## ~ 111 Heroes, 36 ea faction + 1 Servant

## ~ (Hybrid)/Faction (A conjurer like hero, with unique faction-spread combo skills)

<a href="http://tinypic.com?ref=ml347n" target="_blank"><img src="http://i68.tinypic.com/ml347n.jpg" border="0" alt="Image and video hosting by TinyPic"></a>

~ Around once a week or more, the world is extended on its `outskirts`, creating harder and harder experiences, requiring collaboration of players. Most areas are PvP, some are not.

<a href="http://tinypic.com?ref=261f677" target="_blank"><img src="http://i67.tinypic.com/261f677.jpg" border="0" alt="Image and video hosting by TinyPic"></a>


## ~ 3 Factions  

<a href="http://tinypic.com?ref=290rk2t" target="_blank"><img src="http://i66.tinypic.com/290rk2t.jpg" border="0" alt="Image and video hosting by TinyPic"></a>


<a href="http://tinypic.com?ref=jhfoxw" target="_blank"><img src="http://i64.tinypic.com/jhfoxw.jpg" border="0" alt="Image and video hosting by TinyPic"></a>

## ~ Area PvP manipulation prevention
<a href="http://tinypic.com?ref=2e49jmf" target="_blank"><img src="http://i67.tinypic.com/2e49jmf.jpg" border="0" alt="Image and video hosting by TinyPic"></a>


### world 
Known as the **World** update, open world with ressources to gather inside will be released.  ~~
Goal is to achieve open world standard: infinite flat earth, many layers of coordinates, fully editable (building) and biomes. Additionaly, we will emulate wildlife with real state of fauna and flora.  ~~
XP give `humans` the right to claim some land at a fixed rate of 1XP -> 100 surface blocks. ~~
You can find dungeons, kill bosses and gain loot and XP. ~~

~ World is initially a set size, then it increases when we are able to accomodate the time and resources required. See above for faction, PvE expanding concepts

~ 

### Trade 
~~ The **Deal** update will add `humans` market places to the game all around the `earth` world.

~ Trades are offered via a global messaging system, trades must be conducted in person, directly between players. It is up to the traders to determine the meeting spot, safe or not. 

~ There will be a skill in player's `Char UI` which is relative to trades `Marketing` which is just a social score rating for your previous trades. Players can vote for good or bad trades and it determines the experience gained in the `Marketing` skill.

<a href="http://tinypic.com?ref=2vl5h4x" target="_blank"><img src="http://i64.tinypic.com/2vl5h4x.jpg" border="0" alt="Image and video hosting by TinyPic"></a>

### Tools
The **Make** update will bring crafting and machinery into the game. It will give `humans` ability to craft **items** from ressouces and to combine items with **tools** to make better items. New receipes will be released slowly as the game grow and the cities regroup more and more `humans`. ~~

~ 4 skills in `Char UI` which determine crafting of different types of items

<a href="http://tinypic.com?ref=eg97c7" target="_blank"><img src="http://i63.tinypic.com/eg97c7.jpg" border="0" alt="Image and video hosting by TinyPic"></a>

### Social
~~ **Together!**, yes because it's better. A concept of groups will be implemented. They can share ownership of items and locations, group several `humans` together, take decisions together with ingame interface. It's an alliance.

~ Production & Treason, two opposing actions that are voted for with a specific token (Not XP or DTHC), an in-game minor-token that is used for decision making, example name; ĐT

<a href="http://tinypic.com?ref=2uzs905" target="_blank"><img src="http://i65.tinypic.com/2uzs905.jpg" border="0" alt="Image and video hosting by TinyPic"></a>

### New assets
**Create** will give the ability to players to create new designs they will be able to sell to other players. Players can then create a skin for a sword, find a craftman associate and sell the new skined sword to players all around the world. This way, creators can earn XP and guarantee intellectual property. ~~

~ ^ Your idea is really good, it is a very great concept to incorporate with `Marketing` skill perhaps, and people will earn experience that way aswell along with maybe ĐT token for voting elsewhere in-game. These creations are then synced to our external chain for players to use anyway they want!


--------------------------
## Decentralized economy (dEco) - Blockchain role
~~ The blockchain will be used to have a public, immutable state of the economy and a decentralized processing of `humans` skills state, items ownership, crafting processing and land ownership.  

~ Good, good & good. We'll index a players `Char UI` and sync it to a highscores database

~~ Kapital is the owner of intellectual property of the XP blockchain. In this way, no other company could fork and become the central bank.
~~ If dEarth has to be stopped for any legal reason, the blockchain would fall in the public domain and any company could fork it to be the central bank. 

~ Dappital/Dapital, who cares anything i'm just looking to make a pattern of these kind of names, doesn't matter if you don't want to use
~ If dEarth has to be stopped for any legal reason, the blockchain would fall in the public domain and any company could fork it to use the item/skill/char data within their own games, but PvE states are not written to the chain to conserve transaction expenditure losses, they are a LIVE environment

--------------------------
## XP stability
### Challenge
~~ handling a real economy is hard, you have to take care about inflation/deflation and most importantly: **currency value**.  
~~ `Kapital` role is too keep `XP/fiat` rate stable and to keep ingame price correlated to ingame economy.  
~~ You can see `Kapital` as the central bank of `dEarth`.

~  ![Intro to Game Balance](https://gamebalanceconcepts.wordpress.com/2010/07/07/level-1-intro-to-game-balance/)

~ ![Players who suit MUDs](http://mud.co.uk/richard/hcds.htm)

~ Balancing Equipment for Math-hating Game Designers http://spiffspacely.com/balancing-equipment-for-math-hating-game-designers/

~ ![Rethinking gold as currency](https://www.pathofexile.com/forum/view-thread/55102/)

~ ![Greed is good](http://dl.eve-files.com/media/corp/PoeticStanziel/Vol-1-Greed-is-good.pdf)

~ ![Interview with EVE Online economist 1](https://www.rockpapershotgun.com/2014/05/21/eve-economist-interview/)

~ ![Interview with EVE Online economist 2](https://community.eveonline.com/news/dev-blogs/move-over-greenspan/)

### A few paragraphs of MMORPG economic problems
### Economic Systems

First, we use the word “economy” a lot, even in everyday life, so I should define it to be clear. In games, I’ll use the word “economy” to describe any in-game resource. That’s a pretty broad term, as you could take it to mean that the pieces in Chess are a “piece economy” – and I’d argue that yes, they could be thought of that way, it’s just not a particularly interesting economy because the resources can’t really be created, destroyed or transferred in any meaningful way. Most economies that we think of as such, though, have one or more of these mechanics:

    Resource generation, where players craft or receive resources over time
    Resource destruction, where players either burn resources for some use in the game, or convert one type of resource to another.
    Resource trading, where players can transfer resources among themselves, usually involving some kind of negotiation or haggling.
    Limited zero-sum resources, so that one player generating a resource for themselves reduces the available pool of resources for everyone else.

Still, any of those elements individually might be missing and we’d still think of it as an economy.

Like the creation of level design tools, tabletop RPGs, or metrics, creating an economic system in your game is a third-order design activity, which can make it pretty challenging. You’re not just creating a system that your players experience. You’re creating a system that influences player behavior, but then the players themselves are creating another social system within your economic system, and it is the combination of the two that the players actually experience. For example, in Settlers of Catan, players are regularly trading resources between them, but the relative prices of each resource are always fluctuating based on what each individual player needs at any given time (and usually, all the players need different things, with different levels of desperation and different ability to pay higher prices). The good news is that with economies, at least, a lot of human behavior can be predicted. The other good news is that in-game economies have a lot of little “design knobs” for us designers to change to modify the game experience, so we have a lot of options. I’ll be going over those options today.

### Supply and Demand

First, a brief lesson from Economics 101 that we need to be aware of is the law of supply and demand, which some of you have probably heard of. We assume the simplest case possible: an economy with one resource, a really large population of people who produce the resource and want to sell it, and another really large population of people who consume the resource and want to buy it. We’ll also assume for our purposes that any single unit of the resource is identical to any other, so consumers don’t have to worry about choosing between different “brands” or anything.

The sellers each have a minimum price at which they’re willing to part with their goods. Maybe some of them have lower production costs or lower costs of living than others, so they can accept less of a price and still stay in business. Maybe others have a more expensive storefront, or they’re just greedy, so they demand a higher minimum price. At any rate, we can draw a supply curve on a graph that says that for any given price (on the x-axis), a certain number or percentage of sellers are willing to sell at that price (on the y-axis). So, maybe at $1, only two sellers in the world can part with their goods at that price, but at $5 maybe there are ten sellers, and at $20 you’ve got a thousand sellers, and eventually if you go up to $100 every single seller would be willing to sell at that price. Basically, the only thing you need to know about supply curves is that as the price increases, the supply increases; if ten people would sell their good at $5, then at $5.01 you know that at least those ten sellers would still accept (if they sold at $5 then they would clearly sell at $5.01), and you might have some more sellers that finally break down and say, okay, for that extra penny we’re in.

Now, on the other side, it works the same but in reverse. The consumers all have a maximum price that they’re willing (or able) to pay, for whatever reason. And we can draw a demand curve on the same graph that shows for any given price, how many people are willing to buy at that price. And unlike the supply curve, the demand curve is always decreasing; if ten people would buy a good at $5, then at $5.01 you might keep all ten people if you’re lucky, or some of them might drop out and say that’s too rich for their blood, but you certainly aren’t going to find anyone who wouldn’t buy at a lower price but would buy at a more expensive price.

Now of course, in the real world, these assumptions aren’t always true. More teenagers would rather buy $50 shoes than $20 shoes, because price has social cred. And some sellers might not be willing to sell at exorbitantly high prices because they’d consider that unethical, and they’d rather sell for less (or go out of business) than bleed their customers dry. But for our purposes, we can assume that most of the time in our games, supply curves will increase and demand curves will decrease as the price gets more expensive. And here’s the cool part: wherever the two curves cross, will generally turn out to be the actual market price that the players all somehow collectively agree to. Even if the players don’t know the curves, the market price will go there as if by magic. It won’t happen instantly if the players have incomplete information, but it does happen pretty fast, because players who sell at below the current market price will start seeing other people selling at higher prices (because they have to) and say, hey, if they can sell for more than I should be able to also! And likewise, if a consumer pays a lot for something and then sees the guy sitting next to them who paid half what they did for the same resource, they’re going to demand to pay a whole lot less next time.

Now, this can be interesting in online games that have resource markets. If you play an online game where players can sell or trade in-game items for in-game money, see if either the developer or a fansite maintains a historical list of selling prices (not unlike a ticker symbol in the stock market). If so, you’ll notice that the prices change over time slightly. So you might wonder what the deal is: why do prices fluctuate? And the answer is that the supply and demand are changing slightly over time. Supply changes as players craft items and put them on sale, and that supply is constantly changing; and demand also changes, because at any given time a different set of players is going to be online shopping for any given item. You can see this with other games that have any kind of resource buying and selling. And because the player population isn’t infinite, these things aren’t perfectly efficient, so you get unequal amounts of each item being produced and consumed over time.

Now, that points us to another interesting thing about economies: the fewer the players, the more we’ll tend to see prices fluctuate, because a single player controls more and more of the production or consumption. This is why the prices you’ll see for one Clay in the Catan games change a lot from game to game (or even within a single game) relative to the price of some piece of epic loot in World of Warcraft.

Now, this isn’t really something you can control directly as the game designer, but at least you can predict it. It also means if you’re designing a trading board game for 3 to 6 players, you can expect to see more drastic price fluctuations with fewer players, and you might decide to add some extra rules with less players to account for that if a stable market is important to the functioning of your game.

### Multiple resources

Things get more interesting when we have multiple goods, because the demand curves can affect one another. For example, suppose you have two resources, but one can be substituted for another – maybe one gives you +50 health, and the other gives you +5 mana which you can use as a healing spell to get +50 health, so there’s two different items (with similar uses) so if one is really expensive and one is really cheap you can just buy the cheap one. Even if the two aren’t perfect substitutes, players may be willing to accept an imperfect substitute if the price is sufficiently lower than the market value for the thing they actually want, and the price difference between what people will pay for the good and what they’ll pay for the substitute tells you how efficient that substitute is (that is, how “perfect” it substitutes for the original).

On the flip side, you can also have multiple goods where the demand for one increases demand for the other, because they work better if you buy them all as a set (this is sort of the opposite of substitutes). For example, in games where collecting a complete set of matching gear gives your character a stat bonus, or where you can turn in one of each resource for a bonus, a greater demand for one resource pulls up the demand for all of the others… and once a player has some of the resources in the set, their demand for the others will increase even more because they’re already part of the way there.

By creating resources that are meant to be perfect or imperfect substitutes, or several resources that naturally “go together” with each other, you can change the demand (and therefore the market price) of each of them.

### Marginal pricing

As we discussed a long time ago with numeric systems, sometimes demand is a function of how much of a good you already have. If you have none of a particular resource, the first one might be a big deal for you, but if you have a thousand of that resource then one more isn’t as meaningful to you, so demand may actually be on a decreasing curve based on how many of the thing you already have. Or maybe if you can use lots of resources more efficiently to get larger bonuses, it might be that collecting one resource means your demand for more of that resource increases. The same is true on the supply side, where producing lots of a given resource might be more or less expensive per-unit than producing smaller amounts. So you can add these kinds of mechanics in order to influence the price; for example, if you give increasing returns for each additional good that a player possesses, you’ll tend to see the game quickly organize into players going for monopolies of individual goods, as once a player has the majority of a good in the game they’re going to want to buy the rest of them. As an example, if you have decreasing returns for players if they collect a lot of one good, then adding a decreasing cost for producing the good might make a lot of sense if you want the price of that good to be a little more stable.

### Scarcity

I probably don’t need to tell you this, but if the total goods are limited, that increases demand. You see this exploited all the time in marketing, when a company wants you to believe that they only have limited quantities of something, so that you’ll buy now (even at a higher price) because you don’t want to miss your chance. So you can really change the feeling of a game just by changing whether a given resource is limited or infinite.

As an example, consider a first-person-view shooting video game where you have limited ammunition. First, imagine it is strictly limited: you get what you find, but that’s it. A game like that feels more like a survival-horror game, where the player only uses their ammo cautiously, because they never know when they’ll find extra or when they’ll run out. Compare to a game where you have enemies that respawn in each area, random item drops, and stores where you can sell the random drops and buy as much extra ammo as you need. In a game like that, a player is going to be a lot more willing to experiment with different weapons, because they know they’ll get all of their ammo back when they reach the next ammo shop, which makes the game feel more like a typical FPS. Now compare that with a game where you have completely unlimited ammo so it’s not even a resource or an economy anymore, where you can expect the player to be shooting more or less constantly, like some of the more recent action-oriented FPSs. None of these methods is “right” or “wrong” but they all give very different player experiences, so my point is just that you increase demand for a good (and decrease the desire to actually consume it now because you might need it later) the more limited it is.

If the resources of your game drive players towards a victory condition, making the resource limited is a great way to control game length. For example, in most RTS games, the board has a limited number of places where players can mine a limited amount of resources, which they then use to create units and structures on the map. Since the core resources that are required to produce units are themselves limited, eventually players will run out, and once it runs out the players will be unable to produce any more units, giving the game a natural “time limit” of sorts. By adjusting the amounts of resources on the map, you can control when this happens; if the players drain the board dry of resources in the first 5 minutes, you’re going to have pretty short games… but if it takes an hour to deplete even the starting resources near your start location, then the players will probably come to a resolution through military force before resource depletion forces the issue, and the fact that they’re limited at all is just there to avoid an infinite stalemate, essentially to place an upper limit on game length.

With multiplayer games in a closed economy, you also want to be very careful with strictly limited goods, because there is sometimes the possibility that a single player will collect all of a good, essentially preventing anyone else from using it, and you should decide as a designer if that should be possible, if it’s desirable, and if not what you can do to prevent it. For example, if resources do no good until a player actually uses them (and using them puts them back in the public supply), then this is probably not going to be a problem, because the player who gets a monopoly on the good has incentive to spend them, which in turn removes the monopoly.

Open and closed economies

In systems, we say a system is “open” if it can be influenced by things from outside the system itself, and it is “closed” if the system is completely self-contained. Economies are systems, and an open economy has different design considerations than a closed economy.

Most game economies are closed systems; you can generate or spend money within the game, but that’s it, and in fact some people get very uncomfortable if you try to change it to an open system: next time you play Monopoly, try offering one of your opponents a real-world cash dollar in exchange for 500 of their Monopoly dollars as a trade, and see what happens — at least one other player will probably become very upset!

Closed systems are a lot easier to manage from a design standpoint, because we have complete control as designers over the system, we know how the system works, and we can predict how changes in the system will affect the game. Open economies are a lot harder, because we don’t necessarily have control over the system anymore.

A simple example of an open economy in a game is in Poker if additional player buy-ins are allowed. If players can bring as much money as they want to the table, a sufficiently rich player could have an unfair advantage; if skill is equal, they could just keep buying more chips until the luck in the game turns their way. To solve this balance problem, usually additional buy-ins are restricted or disallowed in tournament play.

Another place where this can be a problem is CCGs, where a player spending more money can buy more cards and have a greater collection. Ideally, for the game to be balanced, we would want larger collections to give players more options but not more power, which is why I think rarity shouldn’t be a factor in the cost curve of such a game, at least if you want to maximize your player base. If more money always wins, you set up an in-game economy that essentially has a minimum bar for money to spend if you want to be competitive, and in the real world we also have supply and demand curves, and the higher your initial required buy-in is, the fewer people who will be willing to pay (and thus the smaller your player base).

There are other games where you can buy in-game stuff with real-world cash; this is a typical pattern for free-to-play MMOs and Facebook games, and developers have to be careful with exactly what the player can and can’t buy; if the player can purchase an advantage over their opponents, especially in games that are competitive by nature, that can make the game very unbalanced very quickly. (It’s less of an issue in games like FarmVille where there isn’t really much competition anyway.)

Some designers intentionally unbalance their game in this way, assuming that if they create a financial incentive to gain a gameplay advantage, players will pay huge amounts; and to be fair, some of them do, and if the game itself isn’t very compelling in its core mechanics then this might be the only thing you can fall back on to make money, but if you set out to do this from the start I would call it lazy design. A better method is to create a game that’s actually worth playing for anyone, and then offer to trade money for time (so, maybe you get your next unlock after another couple of hours of gameplay, and the gameplay is fun enough that you can do that without feeling like the game is arbitrarily forcing you to grind… but if you want to skip ahead by paying a couple bucks, we’ll let you do that). In this way, money doesn’t give any automatic gameplay advantage, it just speeds up the progression that’s already there.

One final example of an open economy is in any game, most commonly MMOs, where players can trade or “gift” resources within the game, because in any of those cases you can be sure a secondary economy will emerge where players will exchange real-world money for virtual stuff. Just google “World of Warcraft Gold” and you’ll probably find a few hundred websites where you can either purchase Gold for real-world cash, or sell Gold to them and get paid in cash. There are a few options you can consider if you’re designing an online game like this with any kind of trade mechanic:

    You could just say that trading items for cash is against your Terms of Service, and that any player found to have done so will have their account terminated. This is mostly a problem because it’s a huge support headache: you get all kinds of players complaining to you that their account was banned, and just sending them a form email with the TOS still takes time. In some cases, like Diablo where there isn’t really an in-game trading mechanism and instead the players just drop stuff on the ground and then go pick it up, it can also be really hard to track this. If it’s easy to track (because trades are centralized somewhere), if you really don’t want people to buy in-game goods for cash, you should ask yourself why your trading system that you designed and built even allows it.
    You could say that an open economy is okay, but you don’t support it, so if someone takes your money and doesn’t give you the goods, it’s the player’s problem and not the developer’s. Unfortunately, it is still the developer’s problem, because you will receive all kinds of customer support emails from players claiming they were scammed, and whether you fix it or not you still have to deal with the email volume. If you don’t fix it, then you have to accept you’re going to lose some customers and generate some community badwill. If you do fix it, then accept that players will think of you as a “safety net” which actually makes them more likely to get scammed, since they’ll trust other people by assuming that if the other person isn’t honest, they’ll just send an email to support to get it fixed. Trying to enforce sanctions against scammers is an unwinnable game of whack-a-mole.
    You can formalize trading within your game, including the ability to accept cash payments. The good news for this is that players have no excuses; my understanding is that when Sony Online did this for some of their games, the huge win for them was something like a 40% reduction in customer support costs, which can be significant for a large game. The bad news is that you will want to contact a lawyer on this, to make sure you don’t accidentally run afoul of any national banking laws since you are now storing players’ money.

You’ll also want to consider whether players are allowed to sell their entire character, password and all. For Facebook games this is less of an issue because a Facebook account links to all the games and it’s not so easy for a player to give that away. For an MMO where each player has an individual account on your server that isn’t linked to anything else, this is something that will happen, so you need to decide how to deal with that. (On the bright side, selling a whole character doesn’t unbalance the game.)

In any case, you again want to make sure that whatever players can trade in game does not unbalance the game if a single player uses cash to buy lots of in-game stuff. One common pattern to avoid this is to place restrictions on items, for example maybe you can purchase a really cool suit of armor but you have to be at least Level 25 to wear it.

### Inflation

Now, remember from before that the demand curve is based on each player’s maximum willingness to pay for some resource. Normally we’d like to think of the demand curve as this fixed thing, maybe fluctuating slightly if a different set of players or situations happen to be online, but over time it should balance out. But there are a few situations that can permanently shift the demand curve in one direction or another, and the most important for our purpose is when each player’s maximum willingness to pay increases.

Why would you change the amount you’re willing to pay? Mostly, if you have more purchasing power. If I doubled your income overnight but Starbucks raised the price of its coffee from $5 to $6, if you liked their coffee before you would probably be willing to pay the new price, because you can afford it.

How does this work in games? Consider a game with a positive-sum economy: that is, it is possible for me to generate wealth and goods without someone else losing them. The cash economy in the board game Monopoly is like this, as we’ve discussed before; so is the commodity economy in Catan, as is the gold economy in most MMOs. This means that over time, players get richer. With more total money in the economy (and especially, more total money per player on average), we see what is called inflation: the demand curve shifts to the right as more people are willing to pay higher prices, which then increases the market price of each good to compensate.

In Catan, this doesn’t affect the balance of the game; by the time you’re in the late game and willing to trade vast quantities of stuff for what you need, you’re at the point where you’re so close to winning that no one else is willing to trade with you anyway. In Monopoly the main problem, as I mentioned earlier, is that the economy is positive-sum but the object of the game is to bankrupt your opponents; here we see that one possible solution to this is to change the victory condition to “be the first player to get $2500” or something like that. In MMOs, inflation isn’t a problem for the existing players, because after all they are willing to pay more; however, it is a major problem for new players, who enter the game to find that they’re earning one gold piece for every five hours of play, and anything worth having in the game costs millions of gold, and they can never really catch up because even once they start earning more money, inflation will just continue. So if you’re running an MMO where players can enter and exit the game freely, inflation is a big long-term problem you need to think about. There are two ways to fix this: reduce the positive-sum nature of the economy, or add negative-sum elements to counteract the positive-sum ones.

Negative-sum elements are sometimes called “money sinks,” that is, some kind of mechanism that permanently removes money from the player economy. The trick is balancing the two so that on average, it cancels out; a good way to know this is to actually take metrics on the total sum of money in the game, and the average money per person, and track that over time to see if it’s increasing or decreasing. Money sinks take many forms:

    Any money paid to NPC shopkeepers for anything, especially if that something is a consumable item that the player uses and then it’s gone for good.
    Any money players have to pay as maintenance and upkeep; for example, having to pay gold to repair your weapon and armor periodically.
    Losing some of your money (or items or stats or other things that cost money to replace) when you die in the game.
    Offering limited quantities of high-status items, especially if those items are purely cosmetic in nature and not something that gives a gameplay advantage, which can remove large amounts of cash from the economy when a few players buy them.
    While I don’t know of any games that do this, it works in real life: have an “adventurer’s tax” that all players have to pay as a periodic percentage of their wealth. This not only gives an incentive to spend, but it also penalizes the players who are most at fault for the inflation. Another alternative would be to actually redistribute the wealth, so instead of just removing money from the economy, you could transfer some money from the richest players and distribute it among the poorest; that on its own would be zero-sum and wouldn’t necessarily fix the inflation problem, but it would at least give the newer players a chance to catch up and increase their wealth over time.

To reduce the positive-sum nature of the economy is a bit harder, because players are used to going out there, killing monsters and getting treasure drops. If you make the monsters limited (so they don’t respawn) then the world will become depopulated of monsters very quickly. If you give no rewards, players will wonder why they’re bothering to kill monsters at all. In theory you could do something like this:

    Monsters drop treasure but not gold, and players can’t sell or trade the treasure that’s dropped; so it might make their current equipment a little better, but that’s about it.
    Players receive gold from completing quests, but only the first time each quest, so the gold they have at any given point in the game is limited. Players can’t trade gold between themselves.
    Players can use the gold to buy special items in shops, so essentially it is like the players have a choice of what special advantages to buy for their character.

One other, final solution here is the occasional server reset, when everyone loses everything and has to start over. This doesn’t solve the inflation problem – a new player coming in at the end of a cycle has no chance of catching up – but it does at least mean that if they wait for everything to reset they’ll have as good a chance as anyone else after the reset.

### Trading

Some games use trading and bartering mechanics extensively within their economies. Trading can be a very interesting mechanic if players have a reason to trade; usually that reason is that you have multiple goods, and each good is more valuable to some players than others. In Settlers of Catan, sheep are nearly useless to you if you want to build cities, but they’re great for settlements. In Monopoly, a single color property isn’t that valuable, but it becomes much more powerful if you own the other matching ones. In World of Warcraft, a piece of gear that can’t be equipped by your character class isn’t very useful to you, no matter how big the stat bonuses are for someone else. By giving each player an assortment of things that are better for someone else than for them, you give players a reason to trade resources.

Trading mechanics usually serve as a negative-feedback loop, especially within a closed economy. Players are generally more willing to offer favorable trades to those who are behind, while they expect to get a better deal from someone who is ahead (or else they won’t trade at all).

There are a lot of ways to include trading in your game; it isn’t as simple as just saying “players can trade”… but this is a good thing, because it gives you a lot of design control over the player experience. Here are a few options to consider:

    Can players make deals for future actions as part of the trade (“I’ll give you X now for Y now and Z later”)? If so, are future deals binding, or can players renege?
        Disallowing future deals makes trades simpler; with future considerations, players can “buy on credit” so to speak, which tends to complicate trades. On the other hand, it also gives the players a lot more power to strike interesting deals.
        If future deals are non-binding, players will tend to be a lot more cautious and paranoid about making them. Think about whether you want players to be inherently mistrustful and suspicious of each other, or whether you want to give players every incentive to find ways of cooperating.
    Can players only trade certain resources but not others? For example, in Catan you can trade resource cards but not victory points or progress cards; in Monopoly you can trade anything except developed properties; in some other games you can trade anything and everything.
        Resource that are tradable are of course a lot more fluid than others. Some resources may be so powerful (like Victory Points) that no one in their right mind would want to trade them, so simply making them untradeable stops the players from ruining the game by making bad trades.
    Can players only trade at certain times? In Catan you can only trade with the active player before they build; in Bohnanza there is a trading phase as part of each player’s turn; in Monopoly you can trade with anyone at any time.
        If players can trade at any time, consider if they can trade at “instant speed” in response to a game event, because sometimes the ability to react to game events can become unbalanced. For example, in Monopoly you could theoretically avoid Income Tax by trading all of your stuff to another player, then taking it all back after landing on the space, and you could offer the other player a lesser amount (say, 5%) in exchange for the service of providing a tax shelter. In short, be very clear about exactly when players can and can’t trade.
        If trading events in the game are infrequent (say, you can only trade every few turns or something), expect trading phases to take longer, as players have had more time to amass tradable resources so they will probably have a lot of deals to make.
        If this is a problem, consider adding a timer where players only have so much time to make deals within the trading phase.
    Does the game require all trades to be even (e.g. one card for one card) or are uneven trades allowed (Catan can have uneven numbers of cards, but at least one per side; other games might allow a complete “gift” of a trade)?
        Requiring even trades places restrictions on what players can do and will reduce the number of trades made, but it will also cause trading to move along faster because there’s less room for haggling, and there’s also less opportunity for one weak player to make a bad trade that hands the game to someone else.
        I could even imagine a game where uneven trades are enforced: if you trade at all, someone must get the shaft.
    Are trades limited in quantity, or unlimited? A specific problem here is the potential for the “kingmaker” problem, where one player realizes they can’t win, but the top two players are in a close game, and the losing player can choose to “gift” all of their stuff to one of the top two players to allow one of them to win. Sometimes social pressure prevents people from doing something like this, but you want to be very careful in tournament situations and other “official” games where the economic incentive of prizes might trump good sportsmanship (I actually played in a tournament game once where top prize was $20, second prize was $10, and I was in a position to decide who got what, so I auctioned off my pieces to the highest bidder.)
    Are trades direct, or indirect? Usually a trade just happens, I give you X and you give me Y, but it’s also possible to have some kind of “trade tax” where maybe 10% of a gift or trade is removed and given to the bank for example, to limit trades. This seems strange – why offer trading as a mechanic at all, if you’re then going to disincentivize it? But in some games trading may be so powerful (if two players form a trading coalition for their mutual benefit, allowing them both to pull ahead of all other players, for example) to the point where you might need to apply some restrictions just to prevent trades from dominating the rest of the game.
    Is there a way for any player to force a trade with another player against their will? Trades usually require both players to agree on the terms, but you can include mechanisms to allow one player to force a trade on another player under certain conditions. For instance, in a set collection game, you might allow a player to able to force-trade a more-valuable single item for a less-valuable one from an opponent, once per game.

### Auctions

Auction mechanics are a special case of trading, when one player auctions off their stuff to the other players, or where the “bank” creates an item out of thin air and it is auctioned to the highest bidding player. Auctions often serve as a self-balancing mechanic in that the players are ultimately deciding on the price of how much something is worth, so if you don’t know what to cost something you can put it up for auction and let the players decide. (However, this is lazy design; auctions work the best when the actual cost is variable, different between players, and situational, so that figuring out how much it’s worth is something that changes from game to game, so that the players are actually making interesting choices each time. With an effect that is always worth the same amount, “auction” is meaningless once players figure out how much it’s worth; they’ll just bid what it’s worth and be done with it.)

Auctions are a very pure form of “willingness to pay” because each player has to decide what they’re actually willing to pay so that they can make an appropriate bid. A lot of times there are meta-considerations: not just “how much do I want this for myself” but also “how much do I not want one of my opponents to get it because it would give them too much power” or even “I don’t want this, but I want an opponent to pay more for it, so I’ll bid up the price and take the chance that I won’t get stuck with it at the end.”

An interesting point with auctions is that normally if the auction goes to the highest bidder, that the item up for auction sells for the highest willingness to pay among all of the bidders – that’s certainly what the person auctioning the item wants, is to get the highest price. But in reality, the actual auction price is usually somewhere between the highest and second highest willingness to pay, and in fact it’s usually closer to the second-highest, although that depends on the auction type: sometimes you end up selling for much lower.

Just as there are many kinds of trading, there are also many kinds of auctions. Here’s a few examples:

    Open auction. This is the type most people think of when they think of auctions, where any player can call a higher bid at any time, and when no other bids happen someone says “going once, going twice, sold.” If everyone refuses to bid beyond their own maximum willingness to pay, the person with the highest willingness will purchase the item for one unit more than the second-highest willingness, making this auction inefficient (in the sense that you’d ideally want the item to go for the highest price), but as we’ll see that is a problem with most auctions.
    Fixed price auction. In turn order, each player is offered the option to purchase the item or decline. It goes around until someone accepts, or everyone declines. This gives an advantage to the first player, who gets the option to buy (or not) before anyone else – and if it’s offered at less than the first player’s willingness to pay, they get to keep the extra in their own pocket, so how efficient this auction is depends on how well the fixed price is chosen.
    Circle auction. In turn order, each player can either make a bid (higher than the previous one) or pass. It goes around once, with the final player deciding whether to bid one unit higher than the current highest bid, or let the other player take it. This gives an advantage to the last player, since it is a fixed-price auction for them and they don’t have to worry about being outbid, so they may be able to offer less than their top willingness to pay.
    Silent auction. Here, everyone secretly and simultaneously chooses their bid, all reveal at once, and highest bid wins. You need to include some mechanism of resolving ties, since sometimes two or more players will choose the same highest bid. This can often have some intransitive qualities to it, as players are not only trying to figure out their own maximum willingness to pay, but also other players’ willingness. If the item for auction is more valuable for you than the other players, you may bid lower than your maximum willingness to pay because you expect other players’ bids to be lower, so you expect to bid low and still win.
    Dutch auction. These are rare in the States as it requires some kind of special equipment. You have an item that starts for bid at high price, and there’s some kind of timer that counts down the price at a fixed rate (say, dropping by $1 per second, or something). The first player to accept at the current price wins. In theory this means as soon as the price hits the top player’s maximum willingness to pay, they should accept, but there may be some interesting tension if they’re willing to wait (and possibly lose out on the item) in an attempt to get a better price. If players can “read” each others’ faces in real-time to try to figure out who is interested and who isn’t, there may be some bluffing involved here.

Even once you decide on an auction format, there are a number of ways to auction items:

    The most common is that there’s a single item up for auction at a time; the top bidder receives the item, and no one else gets anything.
    Sometimes an entire set of items are auctioned off at the same time in draft form: top bid simply gets first pick, then second-highest bidder, and so on. The lowest bidder gets the one thing no one else wanted… or sometimes they get nothing at all, if you want to give players some incentive to bid higher. In other words, even if a player doesn’t particularly want any given item, they may be willing to pay a small amount in order to avoid getting stuck with nothing. Conversely, if you want to give players an incentive to save their money by bidding zero, giving the last-place bidder a “free” item is a good way to do that – but of course if multiple players bid zero, you’ll need some way of breaking the tie.
    If it’s important to have negative feedback on auction wins so that a single player shouldn’t win too many auctions in a row, giving a bonus to everyone who didn’t win (or even just the bottom bidder) for winning the next auction is a way to do that.
    Some auctions are what are called negative auctions because they work in reverse: instead of something good happening to the highest bidder, something bad happens to the lowest bidder. In this case players are bidding for the right to not have something bad happen to them. This can be combined with other auctions: if the top bidder takes something from the bottom bidder, that gives players an incentive to bid high even if they don’t want anything. The auction game Fist of Dragonstones had a really interesting variant of this, where the top bidder takes something from the second highest bidder, meaning that if you bid for the auction at all you wanted to be sure you won and didn’t come in second place! On the other hand, if only one person bids for it, then everyone else is in second place, and the bidder can choose to take from any of their opponents, so sometimes it can be dangerous to not bid as well.

Even once you decide who gets what, there are several ways to define who pays their bid:

    The most common for a single-item auction is that the top bidder pays their bid, and all other players spend nothing.
    If the top two players pay their bid (but the top bidder gets the item and the second-highest bidder gets nothing), making low or medium bids suddenly becomes very dangerous, turning the auction into an intransitive mechanic where you either want to bid higher than anyone else, or low enough that you don’t lose anything. This is most common in silent auctions, where players are never sure of exactly what their opponents are bidding. If you do something like this with an open auction things can get out of hand very quickly, as each of the top two bidders is better off paying the marginal cost of outbidding their opponent than losing their current stake – for example, if you auction off a dollar bill in this way and (say) the top bid is 99 cents and the next highest is 98 cents, the second-highest bidder has an incentive to bid a dollar (which lets them break even rather than lose 98 cents)… which then gives incentive to the 99-cent bidder to paradoxically bid $1.01 (because in such a situation they’re only losing one cent rather than 99 cents), and if both players follow this logic they could be outbidding each other indefinitely!
    The top bidder can win the auction but only pay an amount equal to the second-highest bid. Game theory tells us, through some math I won’t repeat here, that in a silent auction with these rules, the best strategy is to bid your maximum willingness to pay.
    In some cases, particularly when every player gets something and highest bid just chooses first, you may want to have all players pay their bid. If only the top player gets anything, that makes it dangerous to bid if you’re not hoping to win – although in a series of such auctions, a player may choose to either go “all in” on one or two auctions to guarantee winning them, or else they may spread them out and try to take a lot of things for cheap when no one else bids.
    If only the top and bottom bidders pay, players may again have incentive to bid higher than normal, because they’d be happy to win but even happier if they don’t have to lose anything. You may want to force players to bid a certain minimum, so there is always at least something at stake to lose (otherwise a player could bid zero and pay no penalty)… although if zero bids are possible, that makes low bids appear safer as there’s always the chance someone will protect you from losing your bid by bidding zero themselves.
    If everyone pays their bid except the lowest bidder, that actually gives players an incentive to bid really high or really low.

Even if you know who has to pay what bid, you have to decide what happens to the money from the auction:

    Usually it’s just paid “to the bank” – that is, it’s removed from the economy, leading to deflation.
    It could be paid to some kind of holding block that collects auction money, and is then redistributed to one or more players later in the game when some condition is met.
    The winning bid may also be paid to one or more other players, making the auction partially or completely zero-sum, in a number of ways. Maybe the bid is divided and evenly split among all other players. The board game Lascaux has an interesting bid mechanic: each player pays a chip to stay in the auction, in turn order, and on their turn a player can choose instead to drop out of the auction and take all of the chips the players have collectively paid so far. The auction continues with the remaining players, thus it is up to the player if it’s a good enough time to drop out (and gain enough auction chips to win more auctions later) or if it’s worth staying in for one more go-round (hoping everyone else will stay in, thus increasing your take when you drop out), or even continuing to stay in with the hopes of winning the auction.

Lastly, no matter what kind of auction there is, you have to decide what happens if no one bids:

    It could be that one of the players gets the item for free (or minimal cost). If that player is known in advance to everyone, it gives other players an incentive to bid just to prevent someone else for getting an item for free. When the players know that the default state is one of their opponents getting a bonus, it’s often an incentive to open the bidding. If players don’t know who it is (say, a random player gets the item for free) then players may be more likely to not bid, as they have just as good a chance as anyone else.
    As an alternative, the auction could have additional incentives added, and then repeated. If one resource is being auctioned off and no one wants it, a second resource could be added, and then the set auctioned… and then if no one wants that, add a third resource, and so on until someone finally thinks it’s worth it.
    Or, what usually happens is the item is thrown out, no one gets it, and the game continues from there as if the auction never happened.

Needless to say, there are a lot of considerations when setting up an in-game economy! Like most things, there are no right or wrong answers here, but hopefully I’ve at least given you a few different options to consider, and the implications of those.

Solving common problems in multiplayer

This next section didn’t seem to fit anywhere else in the course so I’m mentioning it here, so if it seems out of place, that’s why. In multiplayer free-for-all games where there can only be one winner, there are a few problems that come up pretty frequently, that can either be considered a balance or imbalance depending on the game, but they are things that usually aren’t much fun so you want to be very careful of them.

### Turtling

One problem, especially in war games or other games where players attack each other directly, is that if you get in a fight with another player – even if you win – it still weakens both of you relative to everyone else. The wise player reacts to this by doing their best to not get in any fights, instead building up their defenses to make them a less tempting target, and then when all the other players get into fights with each other they swoop in and mop up the pieces when everyone else is in a weakened state. The problem here is that the system is essentially rewarding the players for not interacting with each other, and if the interaction is the fun part of the game then you can hopefully see where this is something that needs fixing.

The game balance problem is that attacking – you know, actually playing the game – is not the optimal strategy. The most direct solution is to reward or incentivize aggression. A simple example is in the board game RISK, where attackers and defenders both lose armies so you’d normally want to just not attack, so the game goes to great lengths to avoid turtling by giving incentives to attack: more territories controlled means you get more armies next turn if you hold onto them, same for continent bonuses, and let’s not forget the cards you can turn in for armies but you only get a card if you attack.

Another solution is to force the issue by making it essentially impossible to not attack. As an example, Plague and Pestilence and Family Business are both light card games where you draw 1 then play 1. A few cards are defensive in nature, but most cards hurt opponents, and you must play one each turn (choosing a target opponent, even), so before too long you’re going to be forced to attack someone else – it’s simply not possible to avoid making enemies.

Kill the leader and Sandbagging

One common problem in games where players can directly attack each other, especially when it’s very clear who is in the lead, is that everyone by default will gang up on the leader. On the one hand, this can serve as a useful negative feedback loop to your game, making sure no one gets too much ahead. On the other hand, players tend to overshoot (so the leader isn’t just kept in check, they’re totally destroyed), and it ends up feeling like a punishment to be doing well.

As a response to this problem, a new dynamic emerges, which I’ve seen called sandbagging. The idea is that if it’s dangerous to be the leader, then you want to be in second place. If a player is doing well enough that they’re in danger of taking the lead, they will intentionally play suboptimally in order to not make themselves a target. As with turtling, the problem here is that players aren’t really playing the game you designed, they’re working around it.

The good news is that a lot of things have to happen in combination for this to be a problem, and you can break the chain of events anywhere to fix it.

    Players need a mechanism to join forces and “gang up” on a single player; if you make it difficult or impossible for players to form coalitions or to coordinate strategies, attacking the leader is impossible. In a foot race, players can’t really “attack” each other, so you don’t see any kill-the-leader strategies in marathons. In an FPS multiplayer deathmatch, players can attack each other, but the action is moving so fast that it’s hard for players to work together (or really, to do anything other than shoot at whoever’s nearby).
    Or, even if players can coordinate, they need to be able to figure out who the leader is. If your game uses hidden scoring or if the end goal can be reached a lot of different ways so that it’s unclear who is closest, players won’t know who to go after. Lots of Eurogames have players keep their Victory Points secret for this reason.
    Or, even if players can coordinate and they know who to attack, they don’t need to if the game already has built-in opportunities for players to catch up. Some Eurogames have just a few defined times in the game where players score points, with each successive scoring opportunity worth more than the last, so in the middle of a round it’s not always clear who’s in the lead… and players know that even the person who got the most points in the first scoring round has only a minor advantage at best going into the final scoring round.
    Or, even if players can coordinate and they know who to attack, the game’s systems can make this an unfavorable strategy or it can offer other strategies. For example, in RISK it is certainly arguable that having everyone attack the leader is a good strategy in some ways… but on the other hand, the game also gives you an incentive to attack weaker players, because if you eliminate a player from the game you get their cards, which gives you a big army bonus.
    Or, since kill-the-leader is a negative feedback loop, the “textbook solution” is to add a compensating positive feedback loop that helps the leader to defend against attacks. If you want a dynamic where the game starts equal but eventually turns into one-against-many, this might be the way to go.

If you choose to remove the negative feedback of kill-the-leader, one thing to be aware of is that if you were relying on this negative feedback to keep the game balanced, it might now be an unbalanced positive feedback loop that naturally helps the leader, so consider adding another form of negative feedback to compensate for the removal of this one.

### Kingmaking

A related problem is when one player is too far behind to win, but they are in a position to decide which of two other people wins. Sometimes this happens directly – in a game with trading and negotiation, the player who’s behind might just make favorable trades to one of the leading players in order to hand them the game. Sometimes it happens indirectly, where the player who’s behind has to make one of two moves as part of the game, and it is clear to everyone that if they make one move then one player wins, and another move causes another player to win.

This is undesirable because it’s anticlimactic: the winner didn’t actually win because of superior skill, but instead because one of the losing players liked them better. Now, in a game with heavy diplomacy (like the board game Diplomacy) this might be tolerable; after all, the game is all about convincing other people to do what you want. But in most games, the winners both feel like the win wasn’t really deserved, so the game designer generally wants to avoid this situation.

As with kill-the-leader, there are a lot of things that have to happen for kingmaking to be a problem, and you can eliminate any of them:

    The players have to know their standing. If no player knows who is winning, who is losing, and what actions will cause one player to win over another, then players have no incentive to help out a specific opponent.
    The player in last place has to know that they can’t win, and that all they can do is help someone else to win. If every player believes they have a chance to win, there’s no reason to give away the game to someone else.
    Or, you can reduce or eliminate ways for players to affect each other. If the person in last place has no mechanism to help anyone else, then kingmaking is impossible.

### Player elimination

A lot of two-player games are all about eliminating your opponent’s forces, so it makes sense that multi-player games follow this pattern as well. The problem is that when one player is eliminated and everyone else is still playing, that losing player has to sit and wait for the game to end, and sitting around not playing the game is not very fun.

With games of very short length, this is not a problem. If the entire game lasts two minutes and you’re eliminated with 60 seconds left to go, who cares? Sit around and wait for the next game to start. Likewise, if player elimination doesn’t happen until late in the game, this is not usually a problem. If players in a two-hour game start dropping around the 1-hour-50-minute mark, relatively speaking it won’t feel like a long time to wait until the game ends and the next one can begin. It’s when players can be eliminated early and then have to sit around and wait forever that you run into problems.

There are a few mechanics that can deal with this:

    You can change the nature of your player elimination, perhaps disincentivizing players to eliminate their opponents, so that the only time a player will actually do this is when they feel they’re strong enough to eliminate everyone and win the game. The board game Twilight Imperium makes it exceedingly dangerous to attack your opponents because a war can leave you exposed, thus players tend to not attack until they feel confident that they can come out ahead, which doesn’t necessarily happen until late game.
    You can also change the victory condition, removing elimination entirely; if the goal is to earn 10 Victory Points, instead of eliminating your opponents, then players can be so busy collecting VP that they aren’t as concerned with eliminating the opposition. The card game Illuminati has a mechanism for players to be eliminated, but the victory condition is to collect enough cards (not to eliminate your opponents), so players are not eliminated all that often.
    One interesting solution is to force the game to end when the first player is eliminated; thus, instead of the victory being decided as last player standing, victory is the player in best standing (by some criteria) when the first player drops out. If players can help each other, this creates some tense alliances as one player nears elimination; the player in the lead wants that player eliminated, while everyone else actually wants to help that losing player stay in the game! The card game Hearts works this way, for example. The video game Gauntlet IV (for Sega Genesis) also did something like this in its multiplayer battle mode, where as soon as one player was eliminated a 60-second countdown timer started, and the round would end even if several players were still alive.
    You can also give the eliminated players something to do in the game after they’re gone. Perhaps there are some NPCs in the game that are normally moved according to certain rules or algorithms, but you can give control of those to the eliminated players (my game group added this as a house rule in the board game Wiz-War, where an eliminated player would take control of all monsters on the board). Cosmic Encounter included rules for a seventh player beyond the six that the game normally supports, by adding “kibbutzing” mechanics where the seventh player can wander around, look at people’s hands, and give them information… and they have a secret goal to try to get a specific other player to win, so while they are giving away information they also may be lying. In Mafia/Werewolf and other variants, eliminated players can watch the drama unfold, so even though they can’t interact the game is fun to observe, so most players don’t mind taking on the “spectator” role.


### Ingame price
All items are tradables for XP cryptocurrency. All prices ingame will be displayed on local currency (¥, €, $, £). Not so RP but it's a political and ethical choice. We all know that kiddos will play this game one day and I want them to understand it's real money.  
I have 2 little brothers and I want to protect any child as I want to protect them.

### XP stability  
- cryptobubble / cryptokrach ~~
- peak of new `humans` -> monetary mass too small ~~
- less `humans` or too much gold farmers -> monetary mass too big ~~

~
- cryptobubble / cryptokrach - not a worry, especially if keeping regular backups
- peak of new `humans` -> monetary mass too small - something the economy has to deal with, players are then forced to react with PvE for economic creation
- less `humans` or too much gold farmers -> monetary mass too big - we will have detection methods, and be honest with the public about botters, but not our detection methods, these kind of things have easy work arounds, but sometimes gold-farmers coordinate with legitimate players, where they buy their gold & dropship it directly to 3rd party buyers, this would be something that we would not want to ban players for... Gold farming is not the same nowadays as it used to be, mostly it is companies with landing pages who sell to players who find them through SEO optimization. Bots are rather easily detected and continously wasted money for "gold farmers" so most now just have direct contact with legitimate players who are willing to sell their in-game items for a specified price, the "gold-farmers" just mark that price up a little bit.

~~ The worst consequence would be `XP` price increase which would result in `dEco` costs too much money to run because it relies on smart contracts. 

~ As afformentioned, I don't believe transaction based actions within game are practical in any sense. dEco cannot be destroyed by limitations from smart contract prices, because the main deco chain is external, and published via HTML for players to view. Other than perhaps ONLY the voting for creation/destruction of structures with a seperate in-game ĐT token.


* When monetary mass is too small, `Kapital` will sell pre mined `XP` at a caped fixed rate and increase loot in basic ressources and `XP`. By fixing a maximum `XP/fiat` rate, `XP` cannot be a speculative crypto. ~~
* When monetary mass is too big, loot rate will be decreased and services price will increase in order to reduce monetary mass.  ~~

### Good ideas
~ ^This is a feasible idea to allow for cost reduction of development, but we don't want to make it so the early birds get too much advantage
~ ^This is also a feasible idea to allow for deflation methods

~~ By thoses 2 mechanisms, `XP` rate cannot be correlated to bitcoin and thus is protected from cryptobubbles/krachs.

~ Everything is always correlated to bitcoin, there is no escaping that. Our best campaign is to always be honest with transactions, and even inform XP buyers, that they will not be given a HUGE advantage, only just for experience


--------------------------
## Blockchain 
### ICO and bonuses
~~ To reward earliest investors whom take the more risks, every step will offer a 100% bonus on the next one: 

~~ 1 XPa = 2XPb = 4XPc and so on until we release the blockchain and the XP cryptocurrency.

~~ *dEarth* company will handle the swap with some reserved XPb tokens.  

~~ Tokens will only be used for fund raising and they will be then swaped for **XP** by *dEarth*.  

~ All seems pretty well conceived here, transferring the ICO drop into experience

### dEarth blockchain
~~ In order to be able to process smart contracts with the XP cryptocurrency, the game world needs its own blockchain with XP as gas for smart contracts processing.  
~~ *dEarth* will run all the necessary nodes to handle charge and as blockchain is open sourced, private nodes will appear if gas price is correctly balanced.

~ As afformentioned and we discussed, in-game chains are not practical for the time being, and would at least require Metamask integration. External for `CharUI` indexing, and also all other Item type assets.

 
### pre-mining of XP
~~ XP will BE premined in order to offer loot in game and most important of all: stability. <----- I understand this and the presale of XP for trading into experience, but below I am confused about the "infinite amount of XP" Shouldn't we just have ONE main Token asset for the ICO? Then that token asset can be transformed into in-game experience? Or am not understanding something?
~~ Premining will allow *dEarth* to hold a virtualy infinite amount of `XP`. <----- Please explain this to me. Because XP & experience have to be entirely seperate. In-game experience cannot have a value on a chain because it would reach the token limit. It is just a value on the `CharUI`.

"`XP` refers at `dEarth` cryptocurrency used to buy and process smart contracts" I think I am confusing myself beceause your initial concept is to use in-game smart contracts, but like discussed, and I hope you still kind of agree, it's just not practical. Otherwise we wouldn't be able to have real-time actions? A player would process an action and it would take sometimes hours to finish the action?

Therefore, XP/DTHC is used PRIMARILY for microtransactions, community developmental input via skin creation chain syncing, and many other aspects that would require smart contracts. We have to really sit down and chat about the considerations for the tokens.



### Token wizard
~~ The ICO has been done thanks to token wizard:  
~~ https://wizard.poa.network/crowdsale?addr=0x2580D616250419cBB435b32173AaFe66e468aCfd&networkID=1

~ Yes I've used that specific generator before too, sounds good


--------------------------
## Technology~~
### open source~~
* Game client ~~
* Api ~~
* Centralized economy of alpha version ~~
* Blockchain ~~
* State resync between *earth* and the blockchain ~~
* Decentralized economy ( ERC20/ERC1155 on the blockchain ) ~~
* Looting algoritm ~~
* wildlife emulation algorithm ~~

~ all sounds good in theory, let's get some ideas to collaborate with for the implementation

### proprietary ~~
* Authoritarive game server ( game state sync client/server ) - interactions humans / earth ~~
* Loot basic items and xp - dispatch xp ~~
* game assets ~~
* services ( marketplace / design creation & patent / skills / betting ) ~~
* `humans` management ~~
* map handling ~~

~ Yep, I'm not sure if I wrote about this earlier, but I specifically mentioned to myself at least, that some aspects HAVE to be closed-source

### Community driven~~
Creation of: ~~
* skills ~~
* items  ~~
* craft recipes ~~
* item skins ~
* voting for structure creation/destruction ~


--------------------------

## Management ~~
### Staff  ~~
* `Kapital` staff:  ~~
~~ Charged to develop the prorietary technologies, they are hired by `Kapital` company.  
* Contributors:  ~~
~~ Paid as freelances, they are in charge to develop open source technologies with `Kapital` staff. 

### Finances ~~
~~ The `Kapital` budget will be public and 100% transparent. Salaries will be defined all together to ensure trust and efficiency.

### Keys Productivity Indicators ~~
* euros invested - 150 EUR so far ~~
* hours spent - 85h so far ~~

~ Will add all info here for every contributor

--------------------------
## Roadmap ~~
There is differents phases for developing the game and decentralizing its economy.  
the alpha stage represents the prototyping of the game: pvp first then open world. Decentralized features will be first developed as centralized while we work on blockchain technology. We want to provide RPG features asap while we need to make them safe and efficient before sealing them in a smart contract. A virtual currency, non-buyable will be used to test this features.

The beta stage will start when we decentralize the economy. A new world (new server) will be opened and we will keep the old one as a traditionnal game server. Think it as "ladder" and "non ladder" in Diablo 2 Battle.net.
 
**Step 1. aug 18 - jan 19 ( 4 months )**  
because we all want to play first, I do a prototype of the pvp game first so we can fight and see who is the best.
In parallel, I prepare the token creation with the community.

**Step 2. jan 19 - jun 19 ( 6 months )**  
Once the fund raised, the team will be hired, be sure I will create a dreamteam of highly skilled professional. You can choose them, I don't want the community to be stuck with non optimal personnal choices.

**Step 3. jan 19 - jan 20**  
Implementation of RPG gameplay in inner app state ( open sourced )
This is a proof of concept and a pre work for writing smart contracts in charge of handling real tokens. Refer to them as pre-contracts, they will be used to write the specs of smart contracts and do strong tests on their behaviour.

Team pre-contract
| priority | name | deadline |
| --- | --- | --- |
| 1. | looting - items | end Q1 19 |
| 2. | pve - xp | end Q2 19 |
| 3. | trading | end Q3 19 |
| 4. | crafting - transformation | end Q4 19 |

Team game
Game client and game server improvement and new features.   


**Step 4. jan 20 - jun 20**  
Integration of pre-contracts into blockchain


--------------------------
## FAQ ~~
**Who are you?**   ~~
I'm an over creative person and this make me able to change an unsolvable problem into a solution, it's so rewarding!
My motto is: 1 hour = 1 new idea.
I'm respectful, I believe in non-violent way of interacting. I strongly believe that humanity stands for peace and that it's just a matter of evolving.
I'm confident, optimistic, I don't like to lie. I'm not perfect, I've done many mistakes in my life but I learnt from every of those and try to be a better human day after day.
I love to see people being successful, I love to teach to children and to raise people up. I strongly believe in the power of the group.
I would prefer to hide my name right now as I'm not sure about the legality of this ICO right now. I'm french and I live in Thaïland.

**What is your professional background ?**  
My dream when I was a child was to make a video game. From 12 to 21 I strongly believed that it was impossible to do it because people told me it was too hard. 
At 21 I understood that people beliefs shouldn't be mine and I started to learn code by myself to make a game.  
I always thought it would be easier to make a traditional business first (service app) to have money to invest in video game industry and that's why I waited so long.
At 29, I'm starting the only game I want to develop since ever, this is my plan for the next 10 years, I'll do it with or without the ICO's success.
My dad gave me the passion for understanding how the financial sector works, which he also learnt by himself. #AutodidactAndEntrepreuneursFamily

**Life goals**  
My main life Goal is to help humanity to build a peaceful and free society. I believe that a real and uncorruptible democracy will give us the ability to leave in sustainable peace.
Internet is a gift to humanity and dEarth is a laboratory for building this kind of tools. 
I also would like to get my pilot license, help my mom to be financially safe till the end of her life, create schools all around the globe where we teach children how to work together, how to be curious and interested by life and how to feel intrinsically safe. I want to be part of the human evolution, I want to be a generator of motivation and ideas. 
I want to invest in hyper-productive farms, respectful of their environment so no human has to suffer from hunger anymore. 
We all know it is possible and we shouldn't be afraid to wish the best for everyone, including us.

**Hobbies**  
I play video games since I can hold a joystick, I do prefer open worlds and freedom than scenario and story. Even if Metal Gear Solid was incredibly well directed !
I love planes and piloting, that's what I do the most when I play GTA but doing it in real life is much more fun.
I really enjoy climbing, swimming, walking on the beach. I enjoy every second spent with my girlfriend. I love to watch sunset and listening to the nature's music.
Coding quickly became a game for me so I guess it should also be in this section ;)


**Why to make an internal virtual game state first before releasing blockchain**
I want this project too work, handling a real world economy is a hard challenge and we must do it step by step to fully understand how to decentralize it while having a central authority, what a paradox! I'm not a blockchain expert, remember that many startup fail because they want to grow too quick. We are commited on a long running project for which I have tons of ideas, and you as the people of `earth` will be part of it.

**what proove me you will work on decentralized economy once you finished to develop it internaly**  
Answer is easy: money. If kapital keeps the economy closed, no one will trust it. Using a crypto currency means people can sell what they earned in game which will leverage interest in the game, which means more money for kapital. I strongly believe in hard currency in video games if economy is real, not correclty balanced but real. 
Without a blockchain, it is nearly impossible for people to trust that we are not manipulating economy. Blockchain will give trust into dEco, trust in dEco will bring money to kapital. This project is much more valuable if it's done entirely. It's all about short term vision vs. long term. This project is my dream for many years (building a RPG over a real economy), I will do it anyway you trust me or not.

**what makes me unable to make another game by myself and use your blockchain as world state ?**
It would be great ! Let's do it and connect our worlds ! We can imagine a 2D web based game living in the same world than a 3D standalone PC game. Why not also a RTS to lead the troups ? The purpose of dEco blockchain is to create an "immutable" world which can be used for anyone who wants to make a pvp game. 

**As your cryptocurrency price will be stable, how can I earn money with it?**
Don't forget that we are making a game relying on blockchain technology and not a high volatility tradable crypto to participate in the bubble. The value of the crypto will go higher as more players comes into the game and more crafting smart contracts are created. dEco is a mirrored copy of our capitalistic real world economy without all the human misery. dEco has a PIB and a value because assets are immutable. It's in our benefit to increase value of the currency over time.

**So we could have different forks for differents game states like different servers ?**
Exactly 

**Is the XPA ICO open ?**
Yes, you can buy tokens here.

**Will you allow raiding or items destruction ?**
This is complicated questions because it means transfering contract ownership without paying for XP. It might be easier to introduce bugs. As `dEarth` run with an authoritarive server and some private data, it will be doable to fork the blockchain in case of big problem. 


~ Hi I'm Oz <--- My temporary public name

Professional
Hobbies
Etc. etc.


##ToDo - Add Blank Full Char UI for example look

## Credits 
wordpress
toggl
https://gitlab.com/l1br3/dEarth
https://discordapp.com/channels/471659163748139028/471659163748139030
https://github.com/poanetwork/token-wizard
https://wordpress.org/plugins/ethereumico
https://www.freepik.com
https://realfavicongenerator.net

http://i63.tinypic.com/2mobfx1.png